cwd=$(pwd)
LINE="-alldirs -mapall=$(id -u):$(id -g) localhost"
FILE=/etc/exports
grep -qF -- "$cwd $LINE" "$FILE" || sudo echo "$cwd $LINE" | sudo tee -a $FILE

LINE="nfs.server.mount.require_resv_port = 0"
FILE=/etc/nfs.conf
grep -qF -- "$LINE" "$FILE" || sudo echo "$LINE" | sudo tee -a $FILE > /dev/null

sudo nfsd restart