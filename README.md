# Dockiator by Webiator.

Docker running Nginx, PHP-FPM, MySQL, PHPMyAdmin and Maildev.

## Installation

1. Clone the project using git:
```sh git clone https://bitbucket.org/webiator/dockiator.git .docker ```

2. Create your own .env file
```sh cp .docker/.env.dist .docker/.env ```

3. Edit your .env file and set up your variables.

4. Change to the docker directory and run the containers.
```sh docker-compose up -d ``` An error message may come up telling you that some network does not exist and that you have to run something like ```sh docker network create network_name``` in order to fix it. Do that!

5. (Optional) Add your dev domain to /etc/hosts
```sh echo "127.0.0.1  your-domain.com" >> /etc/hosts ```

6. To stop the containers simply run
```sh docker-compose down ```

## Drupal 8

### Drush

In order to use **drush** run ```sh ./drush ``` followed by the command you want to execute. Here are some usefull examples:

```sh 
./drush cr
./drush cex -y
./drush cim 
```

## Symfony 4

### Console

In order to use **bin/console** run ```sh ./console ``` followed by the command you want to execute. Here are some usefull examples:

```sh 
./console cache:clear 
./console debug:autowiring
./console doctrine:migrations:migrate
```